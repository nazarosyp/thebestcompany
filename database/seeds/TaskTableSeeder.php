<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Task;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = new Task();
        $task->title = "Do advertising";
        $task->user_id = User::where('role_id', 1)->first()->id;
        $task->creator = User::where('role_id', 2)->first()->id;
        $task->filename = 'task.png';
        $task->save();

        $task = new Task();
        $task->title = "Buy cola and chips";
        $task->user_id = User::where('role_id', 1)->first()->id;
        $task->creator = User::where('role_id', 2)->first()->id;
        $task->filename = 'task.png';
        $task->save();
    }
}
