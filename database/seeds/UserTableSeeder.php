<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdminRole = Role::where('role', 'superadmin')->first();
        $adminRole = Role::where('role', 'admin')->first();
        $managerRole = Role::where('role', 'manager')->first();

        $admin = new User();
        $admin->name = 'Superadmin';
        $admin->email = 'superadmin@admin.com';
        $admin->password = bcrypt('12345678');
        $admin->role_id = $superAdminRole->id;
        $admin->save();

        $admin = new User();
        $admin->name = 'Admin Admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('12345678');
        $admin->role_id = $adminRole->id;
        $admin->save();

        $manager = new User();
        $manager->name = 'Manager';
        $manager->email = 'manager@manager.com';
        $manager->password = bcrypt('12345678');
        $manager->role_id = $managerRole->id;
        $manager->save();
    }
}
