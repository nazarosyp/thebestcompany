@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table">
        <thead>
            <tr>
            <th scope="col">Name</th>
            <th scope="col">email</th>
            <th scope="col">role</th>
            <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td id="userRole-{{ $item->id }}">{{ $item->role }}</td>
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary changeStatus" data-user="{{ $item->id }}" data-role="{{ $item->role }}" data-roleid="{{ $item->role_id }}" data-toggle="modal" data-target="#changeRoleModal">
                        Change role
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

      <!-- Modal -->
    <div class="modal fade" id="changeRoleModal" tabindex="-1" role="dialog" aria-labelledby="changeRoleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="changeRoleModalLabel">Change role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        Current role
                        <span class="font-weight-bold" id="currentRole"></span>
                    </div>
                    @include('manage._form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="changeRoleSubmit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('.changeStatus').click(function(){
        //reset
        $('#currentRole').empty();
        $("#roleSelect option").prop('disabled', false);

        var button = $(this);
        var user = $(this).data('user');
        var currentRole = $(this).data('role');
        var currentRoleId = $(this).data('roleid');

        //show current role in modal
        $('#currentRole').append(currentRole);
        //disable current role
        $('#roleSelect').children('option[value="'+currentRoleId+'"]').attr('disabled','disabled');

        $('#changeRoleSubmit').click(function(){
            var role = $('#roleSelect option:selected').val();
            var newRole = $('#roleSelect option:selected')[0].innerText;
            $('#changeRoleModal').modal("hide");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{ route('change-role') }}',
                data: { user, role },
                success: function (response) {
                    $('#userRole-'+user).empty().append(newRole);
                    button.data('role', newRole);
                    button.data('roleid', role);
                    //notification
                    $('.py-4').prepend('<div class="alert alert-success alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>'+response+'</strong>'+
                                        '</div>')
                },
                error: function (response) {
                    //notification
                    $('.py-4').prepend('<div class="alert alert-danger alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>'+response+'</strong>'+
                                        '</div>')
                }
            });
        })
    })
</script>
@endsection
