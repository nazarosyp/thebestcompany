{{ Form::open() }}

    <div class="form-group">
        <label for="role">Role</label>
        {{ Form::select('role', $roles, null, ['class' => 'form-control', 'id' => 'roleSelect']) }}
    </div>

{{ Form::close() }}
