{{ Form::open(array('action' => 'TaskController@store', 'class' => 'dropzone', 'id'=>'mydropzone', 'files' => true)) }}

<div id="messages">

</div>
<div class="form-group">
    <label for="title">Title</label>
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
    @if($errors->has('title'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="manager">Manager</label>
    {{ Form::select('manager', $managers, null,['class' => 'form-control'] ) }}
</div>

{{ Form::submit('Submit', ['id' => 'submitTask', 'class' => 'btn btn-primary']) }}

{{ Form::close() }}
