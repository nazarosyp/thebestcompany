@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{ url('/css/dropzone.css') }}">
    <script src="{{ url('/js/dropzone.js') }}"></script>
</head>

@section('content')
    <div class="container">
        <h4>Create new task</h4>
        @include('tasks._form')
    </div>

    <script type="text/javascript">
        Dropzone.options.mydropzone =
        {
            maxFilesize: 12,
            autoProcessQueue: false,
            addRemoveLinks : true,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                return time+file.name;
            },
            init: function() {
                var myDropzone = Dropzone.forElement(".dropzone");
                $('#submitTask').click(function(e){
                    $('#messages').text('');
                    e.preventDefault();
                    e.stopPropagation();

                    if (!myDropzone.files || !myDropzone.files.length) {
                        $('#messages').prepend('<div class="alert alert-danger alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>Please, upload file</strong>'+
                                        '</div>');
                        $("#messages").attr("tabindex",-1).focus();
                        return false;
                    }
                    if($('#title').val().length == 0) {
                        $('#messages').prepend('<div class="alert alert-danger alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>Please, fill title input</strong>'+
                                        '</div>');
                        $("#messages").attr("tabindex",-1).focus();
                        return false;
                    }
                    myDropzone.processQueue();
                });
                this.on("complete", function(file) {
                    this.removeAllFiles(true);
                    $('.dropzone')[0].reset();
                });
            },
            acceptedFiles: ".jpeg,.jpg,.png,.svg,",
            addRemoveLinks: true,
            maxFiles: 1,
            timeout: 50000,

            success: function(file, response)
            {
                $('#messages').prepend('<div class="alert alert-success alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>'+response+'</strong>'+
                                        '</div>');
            },
            error: function(file, response)
            {
                console.log(response.errors.title[0]);
                $('#messages').prepend('<div class="alert alert-danger alert-block">'+
                                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                                                '<strong>'+response.message+'</strong>'+
                                        '</div>');

               return false;
            }
        };

    </script>
@endsection
