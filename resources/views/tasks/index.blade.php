@extends('layouts.app')

@section('content')

<div class="container">
    <div class="task-list" style="display: flex; flex-wrap: wrap;">
        @if(count($tasks) == 0)
            <div class="alert alert-secondary" role="alert">
                There are no tasks...
                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('superadmin'))
                    <a href="{{ route("tasks.create") }}" class="alert-link">Let`s create one</a>.
                @endif
            </div>
        @else
            @foreach($tasks as $item)
                <div class="card" style="width: 20rem; margin-right: 1rem; margin-bottom: 2rem;">
                    <img src="/images/{{ $item->filename }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Task</h5>
                        <p class="card-text">{{ $item->title }}</p>
                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('superadmin') )
                            <li class="nav-item">
                            @if(Auth()->user()->id == $item->creator)
                                <a href="{{ route('tasks.destroy', ['id' => $item->id]) }}" class="btn btn-danger">Delete</a>
                            @endif
                        @endif
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

@endsection
