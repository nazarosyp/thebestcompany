<?php

namespace App\Http\Middleware;

use Closure;

class CheckForAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user()->hasRole('manager')) {
            return redirect('home');
        }

        return $next($request);
    }
}
