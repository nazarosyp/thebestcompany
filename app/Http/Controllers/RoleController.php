<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function manageUsers()
    {
        $users = DB::table('users')
        ->select('users.*', 'roles.role')
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->where('role', '!=', 'superadmin')
        ->get();

        $roles = DB::table('roles')
        ->where('role', '!=', 'superadmin')
        ->get(['role', 'id'])
        ->pluck('role', 'id');

        return view('manage.index', compact('users', 'roles'));
    }

    public function changeRole(Request $request){
        if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('superadmin') ){
            $user = User::find($request->get('user'));
            $role = Role::find($request->get('role'));
            $user->role_id = $role->id;
            $user->update();

            return response()->json('Role changed successfully!', 200);
        } else {
            return response()->json('not Found', 500);
        }
    }
}

