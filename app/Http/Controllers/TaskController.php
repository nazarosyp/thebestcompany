<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Task;
use App\User;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        if($user->hasRole('admin')) {
            $tasks = Task::where('creator', $user->id)->get();
        } elseif($user->hasRole('manager')) {
            $tasks = Task::where('user_id', $user->id)->get();
        } else {
            $tasks = [];
        }
        return view('tasks.index', compact('tasks'));
    }

    public function create()
    {
        $managers = DB::table('users')
        ->join('roles', 'users.role_id', '=', 'roles.id')
        ->where('role', '=', 'manager')
        ->get(['users.name', 'users.id'])
        ->pluck('name', 'id');

        return view('tasks.create', compact('managers'));
    }

    public function store(Request $request)
    {
        $rules = [
            'manager' => 'required|exists:users,id',
            'title' => 'required|string|max:255',
            'file' => 'required|mimes:jpeg,jpg,png,svg|max:10000'
        ];
        request()->validate($rules);

        $task = new Task();
        $task->title = $request->get('title');
        $task->user_id = User::find($request->get('manager'))->id;
        $task->creator = auth()->user()->id;

        //save file
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $file->move(public_path('images'),$fileName);

        $task->filename = $fileName;
        $task->save();

        return response()->json('Task created successfully!', 200);
    }

    public function destroy($id)
    {
        $destinationPath = public_path() . '/images/';
        $model = Task::find($id);
        if(auth()->user()->id == $model->creator){
            if(!is_null($model->filename) && file_exists($destinationPath.$model->filename)) {
                unlink($destinationPath.$model->filename);
            }
            $model->delete();
            return back()->with('success', 'Task deleted successfully');
        }else{
            return back()->with('danger', 'not Found');
        }
    }
}
