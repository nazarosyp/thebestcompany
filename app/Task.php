<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'filename', 'creator', 'user_id', 'title'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
