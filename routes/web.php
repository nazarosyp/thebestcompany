<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Tasks
Route::get('/tasks', 'TaskController@index')->name('tasks.index')->middleware('auth');
Route::get('/task/create', 'TaskController@create')->name('tasks.create')->middleware('auth', 'admin');
Route::post('/task/store', 'TaskController@store')->name('tasks.store')->middleware('auth', 'admin');
Route::get('/task/delete/{id}', 'TaskController@destroy')->name('tasks.destroy')->middleware('auth', 'admin');

//Manage users
Route::get('/manage', 'RoleController@manageUsers')->name('manage.index')->middleware('auth', 'admin');
Route::post('/change-role', 'RoleController@changeRole')->name('change-role')->middleware('auth', 'admin');
